import pickle
import timeit
from math import log

from symspellpy import SymSpell

from .classes.indexit import Indexer, NlpAssist
from .classes.parser import Parser
from .classes.searcher import Searcher
from .config import RANKINGFILE, VOCABFILE, SYMSPELLFILE, TERMDOCFILE, BM25B, DOCLENGTHFILE, BM25LDELTA, BM25K1


class Engine:
    def __init__(self):
        self.docs               = None
        self.vocab              = None
        self.bm25l              = None
        self.searcher           = None
        self.parser             = Parser()
        self.indexer            = None
        self.spell_checker      = SymSpell(max_dictionary_edit_distance=4, prefix_length=14)
        self.nlp_assist         = NlpAssist()
        self.term_docs          = None
        self.doc_length         = None
        self.doc_length_sum     = 0
        self.load()

    def load(self):
        try:
            start = timeit.default_timer()
            self.docs = self.parser.load()
            end = timeit.default_timer()
            print("load docs in %d" % (end - start))
        except FileNotFoundError:
            self.parse()

        try:
            start = timeit.default_timer()
            with open(RANKINGFILE, 'rb') as g:
                self.bm25l = pickle.load(g)
            end = timeit.default_timer()
            print("load BM25L in %d" % (end - start))
            print("BM25L size %d" % (len(self.bm25l)))
            start = timeit.default_timer()
            with open(TERMDOCFILE, 'rb') as h:
                self.term_docs = pickle.load(h)
            end = timeit.default_timer()
            print("load term-doc in %d" % (end - start))
            start = timeit.default_timer()
            with open(DOCLENGTHFILE, 'rb') as f:
                self.doc_length = pickle.load(f)
            for v in self.doc_length.values():
                self.doc_length_sum += v
            end = timeit.default_timer()
            print("load term-doc in %d" % (end - start))
        except FileNotFoundError:
            self.index()

        try:
            start = timeit.default_timer()
            flag = self.spell_checker.load_pickle(SYMSPELLFILE)
            with open(VOCABFILE, 'rb') as f:
                self.vocab = pickle.load(f)
            if not flag:
                for k, v in self.vocab.items():
                    self.spell_checker.create_dictionary_entry(k, v.freq)
                self.spell_checker.save_pickle(SYMSPELLFILE)
            end = timeit.default_timer()
            print("load vocab in %d" % (end - start))
        except FileNotFoundError:
            with open(VOCABFILE, 'rb') as f:
                self.vocab = pickle.load(f)
            for k, v in self.vocab.items():
                self.spell_checker.create_dictionary_entry(k, v.freq)
            self.spell_checker.save_pickle(SYMSPELLFILE)

        self.searcher = Searcher(self.docs, self.spell_checker, self.bm25l)

    def search(self, query):
        return self.searcher.search(query)

    def parse(self):
        start = timeit.default_timer()
        self.docs = self.parser.parse()
        end = timeit.default_timer()
        print("parsed in %d" % (end - start))

    def index(self):
        start = timeit.default_timer()
        self.indexer = Indexer(self.docs)
        self.vocab, self.bm25l, self.term_docs = self.indexer.index()
        end = timeit.default_timer()
        print("indexed in %d" % (end - start))

    def update_bm25(self, doc_id, query):
        tokens = self.nlp_assist.clean(query)
        for token in tokens:
            token = self.nlp_assist.lemmatizer.lemmatize(token)
            if token in self.nlp_assist.stopwords_list:
                continue
            key = (token, doc_id)
            td = self.term_docs[key]
            # print(self.bm25l[(token, doc_id)])
            c = (td.tf + 10) / (1 - BM25B + BM25B * (self.doc_length[doc_id] / (self.doc_length_sum / len(self.doc_length))))
            self.bm25l[(token, doc_id)] = (((c + BM25LDELTA) * (BM25K1 + 1)) / (BM25K1 + (c + BM25LDELTA))) * log((len(self.doc_length) + 1) / (self.vocab[token].df + 0.5))
            # print(self.bm25l[(token, doc_id)])


