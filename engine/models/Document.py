import re

from bs4 import BeautifulSoup


class Document:
    def __init__(self, url, html):
        self.url    = url
        self.html   = re.sub('(<!--.*?-->)', '', html, flags=re.DOTALL)
        soup = BeautifulSoup(html, 'html.parser')
        for s in soup(['script', 'style']):
            s.decompose()
        self.title = ''
        self.body = ''
        if soup.find('title') is not None:
            self.title = soup.title.text
        if soup.find('body') is not None:
            self.body = soup.body.text
