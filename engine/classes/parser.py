import pickle
import xml.etree.ElementTree as ET
from os import listdir
from os.path import join, dirname

from ..config import PARSEDDATAFILE
from ..models.Document import Document


class Parser(object):
    def __init__(self):
        self.docs = dict()

    def parse(self):
        path = dirname(__file__) + '/../WEBIR_S'
        files = [join(path, f) for f in listdir(path)]
        cc = 0
        for f in files:
            cc += 1
            tree = ET.parse(f)
            root = tree.getroot()
            for elem in root:
                url, doc_id, html = '', '', ''
                for sub_elem in elem:
                    if sub_elem.tag == "URL":
                        url = sub_elem.text
                    if sub_elem.tag == "DOCID":
                        doc_id = sub_elem.text
                    if sub_elem.tag == "HTML":
                        html = sub_elem.text
                self.docs[doc_id] = Document(url, html)
        self.save()

        return self.docs

    def save(self):
        with open(PARSEDDATAFILE, 'wb') as f:
            pickle.dump(self.docs, f)

    def load(self):
        with open(PARSEDDATAFILE, 'rb') as f:
            self.docs = pickle.load(f)
        return self.docs
