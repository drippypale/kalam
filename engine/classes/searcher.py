import bisect
import re
import timeit

from .indexit import NlpAssist


class Searcher:
    def __init__(self, docs, spell_checker, bm25l):
        self.docs           = docs
        self.spell_checker  = spell_checker
        self.bm25l     = bm25l
        self.nlp_assist     = NlpAssist()
        self.result         = dict()

    def search(self, query):
        self.result.clear()
        segmentation    = self.spell_checker.word_segmentation(query)

        tokens = self.nlp_assist.clean(query)
        clean_tokens = list()
        for token in tokens:
            token = self.nlp_assist.lemmatizer.lemmatize(token)
            if token in self.nlp_assist.stopwords_list:
                continue
            clean_tokens.append(token)

        matching_start = timeit.default_timer()
        iii, s, mm = 0, 0, 0
        for key, value in self.bm25l.items():
            iii += 1
            s += value
            if value > mm:
                mm = value
            if key[0] in clean_tokens:
                res = self.result.get(key[1], None)
                if res is not None:
                    res += value
                else:
                    self.result[key[1]] = value
        print(s)
        print(s/iii)
        print(mm)
        matching_end = timeit.default_timer()
        print("matched in : %d" % (matching_end - matching_start))

        query_results = []
        sort_start = timeit.default_timer()
        for key, value in sorted(self.result.items(), key=lambda item: item[1], reverse=True):
            doc = self.docs[key]
            body = doc.body
            arr = [m.start() for m in re.finditer(query, body)]
            if len(body) > 0 and len(arr) > 0:
                mx = (0, 0)
                ln = 160
                off = 20
                for i in range(len(arr)):
                    j = bisect.bisect_left(arr, arr[i] + ln)
                    if j - i > mx[0]:
                        mx = (j - i, i)
                beg = body[: max(0, arr[mx[1]] - off)].rfind(' ') + 1
                end = body[: min(len(body), arr[mx[1]] + ln + off)].rfind(' ')
                sub = body[beg: end]
                sub = sub.replace(query, '<b>' + query + '</b>')
                description = '...' + sub + '...'
            else:
                description = body[: 100] + '...'
            query_results.append({
                'title': doc.title,
                'url': doc.url,
                'description': description,
                'id': key
            })
        sort_end = timeit.default_timer()
        print("sorted in : %d" % (sort_end - sort_start))
        return {'results': query_results, 'spell-corrected': segmentation.corrected_string if segmentation.corrected_string != query else ''}
