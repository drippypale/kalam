import pickle
import re

from ..config import BODYWEIGHT, HEADWEIGHT, ZIPFLAWLOWBAND, BM25K1, BM25B, VOCABFILE, RANKINGFILE, BM25LDELTA, \
    TERMDOCFILE, DOCLENGTHFILE

from hazm import Normalizer, word_tokenize, Lemmatizer, stopwords_list

from ..models.Term import Term
from ..models.TermDocument import TermDocument
from math import log


class NlpAssist:
    def __init__(self):
        self.normalizer = Normalizer()
        self.lemmatizer = Lemmatizer()
        self.stopwords_list = stopwords_list()

    def clean(self, text, tokenize=True):
        text = self.remove_symbols(text)
        text = re.sub('\s+', ' ', text).strip()
        text = text.lower()
        text = text.replace('\u200c', ' ').replace('\n', '').replace('\r', '').replace('ي', 'ی').replace('ك', 'ک')
        if tokenize:
            text = self.normalizer.normalize(text)
            return word_tokenize(text)
        else:
            return text

    def remove_symbols(self, text):
        text = text.replace('.', ' ').replace('؟', ' ').replace('?', ' ').replace(')', ' ').replace('(', ' ').replace(
            '»', ' ')
        text = text.replace('«', ' ').replace('<', ' ').replace('>', ' ').replace('،', ' ').replace('-', ' ').replace(
            '|', ' ')
        text = text.replace('[', ' ').replace(']', ' ').replace('{', ' ').replace('}', ' ').replace(',', ' ').replace(
            '/', ' ')
        text = text.replace('؛', ' ').replace('+', ' ').replace('!', ' ').replace('ء', ' ').replace('_', ' ').replace(
            ';', ' ')
        text = text.replace('\u200f', ' ').replace('\u200d', ' ').replace('=', ' ').replace(':', ' ') \
            .replace('–', ' ').replace('*', ' ')
        text = text.replace('«', ' ').replace('»', ' ').replace('\'', ' ')
        return text


class Indexer:
    def __init__(self, docs):
        self.docs       = docs
        self.vocab      = dict()
        self.term_docs  = dict()
        self.bm25l = dict()
        self.doc_length = dict()
        self.doc_length_sum = 0
        self.nlp_assist = NlpAssist()

    def index(self):
        mark = dict()
        for doc_id, doc in self.docs.items():
            mark.clear()

            title_tokens = self.nlp_assist.clean(doc.title)
            body_tokens = self.nlp_assist.clean(doc.body)

            for token in body_tokens:
                token = self.nlp_assist.lemmatizer.lemmatize(token)
                if token in self.nlp_assist.stopwords_list:
                    continue
                term = self.token_to_term(token, mark)
                self.term_to_term_document(term, doc_id, BODYWEIGHT)
            for token in title_tokens:
                token = self.nlp_assist.lemmatizer.lemmatize(token)
                if token in self.nlp_assist.stopwords_list:
                    continue
                term = self.token_to_term(token, mark)
                self.term_to_term_document(term, doc_id, HEADWEIGHT)
            temp = len(body_tokens) + len(title_tokens)
            self.doc_length[doc_id] = temp
            self.doc_length_sum += temp

        """
        zipflaw: remove low frequency terms
        """
        low_freq_keys = []
        for k, v in self.vocab.items():
            if v.freq == ZIPFLAWLOWBAND:
                low_freq_keys.append(k)
        for k in low_freq_keys:
            self.vocab.pop(k)
        """
        generate BM25L dict
        N: Number of documents
        """
        N = len(self.docs)
        for td in self.term_docs.values():
            if not self.vocab.get(td.term_token, False):
                continue
            c = td.tf / (1 - BM25B + BM25B * (self.doc_length[td.doc_id] / (self.doc_length_sum / N)))
            self.bm25l[(td.term_token, td.doc_id)] = (((c + BM25LDELTA) * (BM25K1 + 1)) / (BM25K1 + (c + BM25LDELTA))) * log((N + 1) / (self.vocab[td.term_token].df + 0.5))

        # temp = self.vocab
        # self.vocab = dict()
        # for k, v in temp.items():
        #     self.vocab[k] = v.freq
        self.save()
        return self.vocab, self.bm25l, self.term_docs

    def term_to_term_document(self, term, doc_id, score):
        key = (term.token, doc_id)
        td = self.term_docs.get(key, None)
        if td is not None:
            td.tf += score
            return td
        else:
            td = TermDocument(term.token, doc_id)
            td.tf += score
            self.term_docs[key] = td
            return td

    def token_to_term(self, token, mark):
        term = self.vocab.get(token, None)
        if term is not None:
            term.freq += 1
            if not mark.get(token, False):
                term.df += 1
                mark[token] = True
            return term
        else:
            term = Term(token)
            term.freq += 1
            term.df += 1
            mark[term] = True
            self.vocab[token] = term
            return term

    def save(self):
        with open(VOCABFILE, 'wb') as f:
            pickle.dump(self.vocab, f)
        with open(RANKINGFILE, 'wb') as g:
            pickle.dump(self.bm25l, g)
        with open(TERMDOCFILE, 'wb') as h:
            pickle.dump(self.term_docs, h)
        with open(DOCLENGTHFILE, 'wb') as f:
            pickle.dump(self.doc_length, f)
