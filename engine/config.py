BODYWEIGHT                      = 3
HEADWEIGHT                      = 12
ZIPFLAWLOWBAND                  = 1
BM25K1                          = 1.3
BM25B                           = 0.75
RANKINGFILE                     = 'BM25L.bin'
VOCABFILE                       = 'vocab.bin'
SYMSPELLFILE                    = 'sym-spell.bin'
PARSEDDATAFILE                  = 'parsed_data.bin'
TERMDOCFILE                     = 'term-docs.bin'
DOCLENGTHFILE                   = 'doc-length.bin'
BM25LDELTA                      = 0.5
