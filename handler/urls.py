from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.search, name='search'),
    path('update/', views.update_bm25l, name="updateBM25")
]