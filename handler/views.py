from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from engine.engine import Engine
import json

engine = Engine()


@csrf_exempt
def search(request):
    if request.method == 'POST':
        query = json.loads(request.body)['search-query']
        res = engine.search(query)
        return JsonResponse(res, safe=False)


@csrf_exempt
def update_bm25l(request):
    if request.method == 'POST':
        query = json.loads(request.body)['search-query']
        doc_id = json.loads(request.body)['doc-id']
        engine.update_bm25(doc_id, query)
        return HttpResponse('OK')
